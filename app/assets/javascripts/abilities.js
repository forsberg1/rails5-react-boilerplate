var Abilities = Class({

    initialize: function(el, options) {
      this.defaults = {
        hideOnLoad: false,
      }

      if (typeof options == 'object') {
          this.options = $.extend(this.defaults, options);
      } else {
          this.options = this.defaults;
      }

      this.$el = el
      this._hideComponentIfStated();
      this._bindEffectsIfStated();
    },
    _hideComponentIfStated: function () {
      if (this.options.hideOnLoad === true)
        this.$el.hide();
    },

    _bindEffectsIfStated: function () {
      if (this.options.hasOwnProperty('on')) {
        var self = this
        var listeners = this.options.on.listeners

        _.each(listeners, function(listener, key) {
          self.$el.on(key, function() {
            self._setEffectForListener(listener)
          })
        })

      }
    },
    _setEffectForListener: function (effect) {
      if (this.$el.hasClass('animated')) {
        this.$el.removeClass('animated')
      }
      this.$el.addClass('animated').addClass(effect)
    }
});

Abilities.prototype.triggerEffect = function (effect) {
  var effects = new Effect(),
  requestedEffect = effects.getEffect(effect)
  this.$el.addClass('animated').addClass(requestedEffect)

  this.repeat = (function () {
    this._unbindEffect(requestedEffect)
    // Add functionality only to trigger effect once in
  })

  return this
};

Abilities.prototype._unbindEffect = function (effect) {
  var self = this
  this.$el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
    self.$el.removeClass('animated ' + effect)
  });
};
