var Button = React.createClass({

  render() {
     // Using https://github.com/JedWatson/classnames
     var className = classNames('btn', this.props.primary && 'btn-primary',
                                this.props.success && 'btn-success',
                                this.props.danger && 'btn-danger',
                                this.props.info && 'btn-info',
                                this.props.warning && 'btn-warning');
     return <button type="button" className={className} onClick={this.props.onClick}>
       {this.props.children}
     </button>;
   }
});
