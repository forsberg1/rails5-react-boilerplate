var NavigationList = React.createClass({
  getInitialState: function() {
    this.component = $('div[data-react-class="NavigationList"]')
    this.abb       = new Abilities(this.component)
    this.effect    = new Effect()
    return {bla: 1};
  },
  componentWillMount: function () {
  },
  componentWillUpdate: function(nextProps, nextState){
  },
  componentDidMount: function () {
  },
  render: function () {
    return (
      <ul className="NavigationList">
      <li>
        <a href={"/"}
           ref="title">
            {I18n.t('navigation.home')}
        </a>
      </li>
        <li>
          <a href={"/account/new"}
             ref="title">
              {I18n.t('navigation.become_member')}
          </a>
        </li>
      </ul>
    );
  }
});
