var fieldValues = {
  email    : null,
  validate_email: null,
  password : null,
  validate_password : null
};

var Registration = React.createClass({
  getInitialState: function () {
    return {error: false}
  },
  handleError: function (message) {
    this.setState({error: true, message: message})
  },
  render: function () {
    return (
      <div>
        <h4>Sign up component</h4>
        <AccountForm fieldValues={fieldValues}
                     saveValues={this.saveValues}
                     handleError={this.handleError} />
      </div>
    )
  },

  saveValues: function(fields) {
    return function() {
      // Remember, `fieldValues` is set at the top of this file, we are simply appending
      // to and overriding keys in `fieldValues` with the `fields` with Object.assign
      // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
      fieldValues = Object.assign({}, fieldValues, fields)
      console.log(fieldValues)
    }()
  },
});
