
var AccountForm = React.createClass({
  getInitialState: function () {
    this.validationRules = Validate.VALIDATE
     return {
       canSubmit: false
     }
   },
   enableButton: function () {
     this.setState({
       canSubmit: true
     });
   },
   disableButton: function () {
     this.setState({
       canSubmit: false
     });
   },
   submit: function(e) {
     //this.props.saveValues(data)
   },
   validateCurrentState: function (e) {
     console.log(e.target)
   },

   getRefs: function () {
     var refs = this.refs
     var data = {
       email    : refs.email.value,
       validate_email: refs.validate_email.value,
       password : refs.password.value,
       validate_password : refs.validate_password.value
     }
     return data
   },

   render: function () {
       return (
         <Formsy.Form className="form-inline" onValidSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton}>
           <Input placeholder={I18n.t('dictionary.email')} type="email" name="email" validations="isEmail" validationError={I18n.t('form.errors.not_an_email')} required/>
           <Input placeholder={I18n.t('dictionary.password')} type="password"  name="password" validations={{
             minLength: this.validationRules.PASSWORD.minLength
           }} validationErrors={{
                minLength: I18n.t('form.errors.incorrect_length_password', {length: 6})
           }}  required/>

           <Input placeholder={I18n.t('dictionary.password_repeat')} type="password"  name="repeated_password" validations="equalsField:password" validationError={I18n.t('form.errors.password_not_match')} required/>

           <button type="submit" disabled={!this.state.canSubmit}>Submit</button>
         </Formsy.Form>

       );
     }


  });
