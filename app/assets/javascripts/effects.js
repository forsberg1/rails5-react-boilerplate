var Effect = Class({
  initialize: function() {
    this.type = {
      slideInDown: 'slideInDown',
      slideOutUp: 'slideOutUp',
      slideInLeft: 'slideInLeft',
      slideInRight: 'slideInRight',
      shake: 'shake'
    }
  },
  getEffect: function (effect) {
    var types = this.type
    if (types.hasOwnProperty(effect))
      return effect
    throw new Error('Non supported effect: ' + effect);
  }

});
