var Validate = {
  VALIDATE: {
    PASSWORD: {
      minLength: 6
    }
  },
  doTheyMatch: function (str1, str2) {
    if (str1 === str2) return true
    return false
  },
  containsNull: function (obj) {
    for (var prop in obj) {
      if (obj[prop] === "") {
        return true
      }
    }
    return false
  }
}
