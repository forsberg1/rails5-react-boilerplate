var FormHelper = {
  InputErrorNotify: function(inputValueObj) {
    var $inputs = inputValueObj

    _.each($inputs, function (data, key) {

      if (data === "") {
        $("input[data-expose-name="+key+"]").addClass('warning')
      } else {
        $("input[data-expose-name="+key+"]").removeClass('warning')
      }

    });
  }
}
