## README

A boilerplate for Rails 5 with React and turbolinks.

Stuff included in this boilerplate:

* Rails 5

* Bootstrap 3 (scss/javascript)

* React

* React-formsy (Form validation plugin for react)

* i18n-js (locale managment inside React components)

* No database managment (It's removed)

How to run:

* Clone repo
* bundle 
* rails s